//
//  CategoryCell.swift
//  coder-swag
//
//  Created by Paula chen on 8/29/17.
//  Copyright © 2017 Paula chen. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    
    //MARK: IBOutlets
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    

    func updateViews(category: Category) {
        categoryImage.image = UIImage(named: category.imageName)
        categoryTitle.text = category.title
    }
   

}
